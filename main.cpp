//https://os.mbed.com/cookbook/m3pi
#include "mbed.h"
#include "m3pi.h"

//https://os.mbed.com/cookbook/m3pi-RPC
m3pi m3pi;

// RN42 module defaults to 115,200 and is connected on p28,p27
//SerialRPCInterface Interface(p28, p27, 115200);

Serial rn42(p28,p27);
DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);
DigitalOut led[] = {led1,led2,led3,led4};
Serial pc(USBTX, USBRX); // tx, rx
PinName encod1A(p21);
PinName encod1B(p22);
PinName encod2A(p24);
PinName encod2B(p23);

float xM;
float yM;
float aMax = 0.07;
float lastXread = 0;
float lastYread = 0;

/*
DigitalIn encod1A(p21);
DigitalIn encod1B(p22);
DigitalIn encod2A(p23);
DigitalIn encod2B(p24);
*/

class Encoder
{
public:
    Encoder(PinName pin, PinName ctr) : _interrupt(pin) {        // create the InterruptIn on the pin specified to Counter
        ctrl = new DigitalIn(ctr);
        _interrupt.rise(this, &Encoder::increment); // attach increment function of this counter instance
        rpm = 0;
        tpr = 32;
        inc_enc_constant = 1./tpr;
        t.start();
        pino = pin;
        sens = true;
    }
    
    void increment() {
        sens = ctrl->read();
        pc.printf("|%d|", ctrl->read());
        delta_time = t.read_us();
        t.reset();
        rpm = ( inc_enc_constant  / (delta_time/1000000) ) *60 ; // rev / m
        _count++;
    }
    
    int read() {
        return _count;
    }
    
    long float getDeltaTime(){
        return delta_time;
    }
    
    float speed(){
      if(sens)
        return -rpm;
      return rpm;
    }
    
    int pino;
    
private:
    int tpr;                // tick / revolution (for #3080 1000:1 Micro Metal Gearmotor HPCB 6V with Extended Motor Shaft)
    float inc_enc_constant;
    InterruptIn _interrupt;
    volatile int _count;
    long float delta_time;  // time between 2 ticks
    Timer t;
    float rpm;              // revolution / minute
    DigitalIn* ctrl;
    bool sens;
};


// only for debug
void printBinary(int x){
    int i;
    for(i = sizeof(x)*8-1; 0 <= i; i --)
        pc.printf("%d ", (x >> i) & 0x01);
}

// get (x,y) from the bluetooth socket
void getXY(float *x, float *y){
  //on vide le buffer, mais c'est plus long
  //while(rn42.readable()){ 
    *y = rn42.getc();
    *y = ((*y-100)*3.)/100.;
    lastYread = *y;
    *x = rn42.getc();
    *x = ((*x-100)*3.)/100.;
    lastXread = *x;
    //pc.printf("getXY (x, y = %f, %f)\n", *x, *y);
  //}
}

void adjust(float *x, float *y){
    
    //if(*x != 0)
    //  pc.printf("adjust1 %f,%f\n", *x, *y);
    if(*x > xM+aMax)
        *x = xM+aMax;
    if(*x < xM-aMax)
        *x = xM-aMax;
    
    if(*y > yM+aMax)
        *y = yM+aMax;
    if(*y < yM-aMax)
        *y = yM-aMax;
    
        pc.printf("adjust2 %f,%f\n", *x, *y);
}

void controlMotors(float x, float y){
    //pc.printf("controlMotors(%f, %f)\n", x, y);
    xM = x;
    yM = y;
    m3pi.left_motor((x-y)/3.); // TODO : check min, max
    m3pi.right_motor((x+y)/3.);
}

// to put on 1 led of the mbed (1 to 4), or put of all with 0
void light(int n){
    //pc.printf("light(%d)\n", n);
    led1 = 0;
    led2 = 0;
    led3 = 0;
    led4 = 0;
    if(n<=4 && n>=1) {
        // classical 4 directions
        //pc.printf("light(%d)", n);
        led[n-1] = 1;
    } else if (n!=0){ // all off if (0,0)
        //pc.printf("light(%d)", n);
        // error case
        led1 = 1;
        led2 = 1;
        led3 = 1;
        led4 = 1;
    }
}

void lightXY(float x, float y){
    if(x==0 && y==0)
        light(0);
    else if(x<0 && y<0)
        light(1);
    else if(x<0 && y>0)
        light(2);
    else if(x>0 && y<0)
        light(3);
    else if(x>0 && y>0)
        light(4);
}

void sendSpeed(char speed){
    rn42.putc(speed);
    rn42.printf("TEST");
    led1 = !led1;
}

int main() {
    
    
    pc.printf("___Start of the program___\n");
    
    //init communication
    rn42.baud(115200);
    
    
    float x = 0;
    float y = 0;
    xM = 0;
    yM = 0;
    
    
    //m3pi.locate(0,1);
    //m3pi.printf("TEST");

    //wait (5.0);
    //BASIC TEST 
    m3pi.forward(0.5); // Forward half speed
    wait (0.5);        // wait half a second
    m3pi.left(0.5);    // Turn left at half speed
    wait (0.5);        // wait half a second
    m3pi.backward(0.5);// Backward at half speed 
    wait (0.5);        // wait half a second
    m3pi.right(0.5);   // Turn right at half speed
    wait (0.5);    
    m3pi.right(0);
    m3pi.backward(0);
    m3pi.left(0);
    m3pi.forward(0);
    
    //check constants (debug)
    //pc.printf("size of char = %d \n", sizeof(char));
    //pc.printf("size of int = %d \n", sizeof(int));
    
    // setup the serial with the bluetooth socket
    rn42.baud(115200);
    
    Encoder* encoder1 = new Encoder(encod1B, encod1A);
    Encoder* encoder2 = new Encoder(encod2B, encod2A);
    
    char send = 0;    
    
    while (1) {
        
        // get direction from serial bluetooth
        if(rn42.readable()){
            getXY(&x, &y);
        } else {
            x = lastXread;
            y = lastYread;    
        }
        
        adjust(&x, &y);
        
        // control the motors according these positions
        controlMotors(x, y);
        
        // light the LED of the mbed according the direction (usefull for debug)
        lightXY(x, y);
        //pc.printf(" x, y = %f, %f\n", x, y);
        
        
        
        //Test encodeurs
        /*led1 = DigitalIn(encod1A);
        led2 = DigitalIn(encod1B);
        led3 = DigitalIn(encod2A);
        led4 = DigitalIn(encod2B);
        */
        
        //pc.printf("Roue1=%f\n", roue1.speed());
        //pc.printf("Roue2=%f\n", roue2.speed());
        
        /*
        //test send speed
        //led1 = 0;
        led2 = 0;
        led3 = 0;
        led4 = 0;
        
        sendSpeed(send);
        send = (send+1)%100;
        
        if(encoder1->speed()<0){
            led1 = 1;
        } else {
            led2 = 1;
        }
        
        if(encoder2->speed()<0){
            led3 = 1;
        } else {
            led4 = 1;
        }
        */
        
        // TODO : 100 to confirm
        //wait_ms(100);
    }
    
    m3pi.stop();
}